package pulsar

const (
	TopicPaymentRequested  = "/payment/requested"
	TopicPaymentSubmitted  = "/payment/submitted"
	TopicPaymentDeclined   = "/payment/declined"
	TopicStoreValidated    = "/payment/store-validated"
	TopicGoodsValidated    = "/payment/goods-validated"
	TopicMerchantValidated = "/payment/merchant-validated"
	TopicPartnerValidated  = "/payment/partner-validated"
)
