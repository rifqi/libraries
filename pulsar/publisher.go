package pulsar

import (
	"context"
	"fmt"
	"time"

	"github.com/apache/pulsar-client-go/pulsar"
	"github.com/rs/xid"
	"google.golang.org/protobuf/proto"
)

type EventPublisher struct {
	Producers map[string]pulsar.Producer
}

func NewEventPublisher(client pulsar.Client, topic []string) (pub *EventPublisher, err error) {
	pub = &EventPublisher{Producers: make(map[string]pulsar.Producer)}

	for _, t := range topic {
		var producer pulsar.Producer
		if producer, err = client.CreateProducer(pulsar.ProducerOptions{
			Topic: t,
		}); err != nil {
			return
		}
		pub.Producers[t] = producer
	}

	return
}

func (ep *EventPublisher) Close(topic string) {
	if p, b := ep.Producers[topic]; b {
		p.Close()
	}
}

func (ep *EventPublisher) CloseAll() {
	for _, v := range ep.Producers {
		v.Close()
	}
}

func (ep *EventPublisher) Publish(topic string, msg *Message, props map[string]string) (err error) {
	if p, err := ep.GetProducer(topic); err != nil {
		return err

	} else {
		props["topic"] = p.Topic()
		payload := ep.NewMessage(msg, props)

		_, err = p.Send(context.Background(), payload)
	}

	return
}

func (ep *EventPublisher) NewMessage(msg *Message, props map[string]string) (message *pulsar.ProducerMessage) {
	if _, ok := props["messageId"]; !ok {
		props["messageId"] = xid.New().String()
	}

	msgProto, err := proto.Marshal(msg)
	if err != nil {
		return
	}

	message = &pulsar.ProducerMessage{
		Payload:    msgProto,
		Properties: props,
		EventTime:  time.Now(),
	}

	if msg.MessageKey != "" {
		message.Key = msg.MessageKey
	}

	return
}

func (ep *EventPublisher) Send(topic string, msg *pulsar.ProducerMessage) (err error) {
	if p, err := ep.GetProducer(topic); err != nil {
		return err
	} else {
		_, err = p.Send(context.Background(), msg)
	}

	return
}

func (ep *EventPublisher) GetProducer(topic string) (p pulsar.Producer, err error) {
	if p, ok := ep.Producers[topic]; ok {
		return p, nil
	} else {
		return nil, fmt.Errorf("Topic not found")
	}
}
