package pulsar

import (
	"log"

	"github.com/apache/pulsar-client-go/pulsar"
)

type (
	PulsarHandlerFunc func(*EventSubscriber, pulsar.ConsumerMessage) error

	EventSubscriber struct {
		Consumer      pulsar.Consumer
		Channel       chan pulsar.ConsumerMessage
		HandlerRouter map[string]PulsarHandlerFunc
	}
)

func NewEventSubscriber(client pulsar.Client, subsName string, subsType pulsar.SubscriptionType, handlerRouter map[string]PulsarHandlerFunc) (sub *EventSubscriber, err error) {
	var topics []string
	for k, _ := range handlerRouter {
		topics = append(topics, k)
	}

	channel := make(chan pulsar.ConsumerMessage, 100)
	options := pulsar.ConsumerOptions{
		Topics:           topics,
		SubscriptionName: subsName,
		Type:             subsType,
		MessageChannel:   channel,
	}

	consumer, err := client.Subscribe(options)
	if err != nil {
		log.Fatal(err)
	}

	sub = &EventSubscriber{
		Consumer:      consumer,
		HandlerRouter: handlerRouter,
		Channel:       channel,
	}

	return
}

func (s *EventSubscriber) Close() {
	s.Consumer.Close()
}

func (s *EventSubscriber) Serve() {
	for cm := range s.Channel {
		msg := cm.Message
		topic := msg.Properties()["topic"]

		if handler, ok := s.HandlerRouter[topic]; ok {
			go handler(s, msg.(pulsar.ConsumerMessage))
		} else {
			s.Consumer.Ack(msg)
		}
	}
}
